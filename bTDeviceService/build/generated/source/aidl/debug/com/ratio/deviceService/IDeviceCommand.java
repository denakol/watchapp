/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: C:\\Users\\denis\\AndroidStudioProjects\\WatchApp\\bTDeviceService\\src\\main\\aidl\\com\\ratio\\deviceService\\IDeviceCommand.aidl
 */
package com.ratio.deviceService;
// interface to the DeviceService

public interface IDeviceCommand extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.ratio.deviceService.IDeviceCommand
{
private static final java.lang.String DESCRIPTOR = "com.ratio.deviceService.IDeviceCommand";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.ratio.deviceService.IDeviceCommand interface,
 * generating a proxy if needed.
 */
public static com.ratio.deviceService.IDeviceCommand asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.ratio.deviceService.IDeviceCommand))) {
return ((com.ratio.deviceService.IDeviceCommand)iin);
}
return new com.ratio.deviceService.IDeviceCommand.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_scanDevices:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String[] _arg0;
_arg0 = data.createStringArray();
int _arg1;
_arg1 = data.readInt();
this.scanDevices(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_startDeviceScan:
{
data.enforceInterface(DESCRIPTOR);
this.startDeviceScan();
reply.writeNoException();
return true;
}
case TRANSACTION_stopDeviceScan:
{
data.enforceInterface(DESCRIPTOR);
this.stopDeviceScan();
reply.writeNoException();
return true;
}
case TRANSACTION_isScanning:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.isScanning();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_connectDevice:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
long _arg1;
_arg1 = data.readLong();
this.connectDevice(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_disconnectDevice:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.disconnectDevice(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_discoverServices:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.discoverServices(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_getServices:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.getServices(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_getCharacteristics:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
this.getCharacteristics(_arg0, _arg1);
reply.writeNoException();
return true;
}
case TRANSACTION_setCharacteristicNotification:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
boolean _arg3;
_arg3 = (0!=data.readInt());
this.setCharacteristicNotification(_arg0, _arg1, _arg2, _arg3);
reply.writeNoException();
return true;
}
case TRANSACTION_readCharacteristic:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
this.readCharacteristic(_arg0, _arg1, _arg2);
reply.writeNoException();
return true;
}
case TRANSACTION_writeCharacteristicString:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
java.lang.String _arg3;
_arg3 = data.readString();
this.writeCharacteristicString(_arg0, _arg1, _arg2, _arg3);
reply.writeNoException();
return true;
}
case TRANSACTION_writeCharacteristicByteArray:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
byte[] _arg3;
_arg3 = data.createByteArray();
this.writeCharacteristicByteArray(_arg0, _arg1, _arg2, _arg3);
reply.writeNoException();
return true;
}
case TRANSACTION_writeCharacteristicInt:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
int _arg3;
_arg3 = data.readInt();
int _arg4;
_arg4 = data.readInt();
int _arg5;
_arg5 = data.readInt();
this.writeCharacteristicInt(_arg0, _arg1, _arg2, _arg3, _arg4, _arg5);
reply.writeNoException();
return true;
}
case TRANSACTION_writeDescriptor:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
java.lang.String _arg3;
_arg3 = data.readString();
byte[] _arg4;
_arg4 = data.createByteArray();
this.writeDescriptor(_arg0, _arg1, _arg2, _arg3, _arg4);
reply.writeNoException();
return true;
}
case TRANSACTION_readDescriptor:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
java.lang.String _arg1;
_arg1 = data.readString();
java.lang.String _arg2;
_arg2 = data.readString();
java.lang.String _arg3;
_arg3 = data.readString();
this.readDescriptor(_arg0, _arg1, _arg2, _arg3);
reply.writeNoException();
return true;
}
case TRANSACTION_isRetrying:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
boolean _result = this.isRetrying(_arg0);
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_getConnectionState:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
int _result = this.getConnectionState(_arg0);
reply.writeNoException();
reply.writeInt(_result);
return true;
}
case TRANSACTION_readRemoteRSSI:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String _arg0;
_arg0 = data.readString();
this.readRemoteRSSI(_arg0);
reply.writeNoException();
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.ratio.deviceService.IDeviceCommand
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void scanDevices(java.lang.String[] advertisedServiceUUIDList, int periodMsec) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeStringArray(advertisedServiceUUIDList);
_data.writeInt(periodMsec);
mRemote.transact(Stub.TRANSACTION_scanDevices, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void startDeviceScan() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_startDeviceScan, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void stopDeviceScan() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_stopDeviceScan, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public boolean isScanning() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_isScanning, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public void connectDevice(java.lang.String deviceAddress, long timeoutMsec) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
_data.writeLong(timeoutMsec);
mRemote.transact(Stub.TRANSACTION_connectDevice, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void disconnectDevice(java.lang.String deviceAddress) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
mRemote.transact(Stub.TRANSACTION_disconnectDevice, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void discoverServices(java.lang.String deviceAddress) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
mRemote.transact(Stub.TRANSACTION_discoverServices, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void getServices(java.lang.String deviceAddress) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
mRemote.transact(Stub.TRANSACTION_getServices, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void getCharacteristics(java.lang.String deviceAddress, java.lang.String serviceUUID) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
_data.writeString(serviceUUID);
mRemote.transact(Stub.TRANSACTION_getCharacteristics, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void setCharacteristicNotification(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, boolean enabled) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
_data.writeString(serviceUUID);
_data.writeString(characteristicUUID);
_data.writeInt(((enabled)?(1):(0)));
mRemote.transact(Stub.TRANSACTION_setCharacteristicNotification, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void readCharacteristic(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
_data.writeString(serviceUUID);
_data.writeString(characteristicUUID);
mRemote.transact(Stub.TRANSACTION_readCharacteristic, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void writeCharacteristicString(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, java.lang.String value) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
_data.writeString(serviceUUID);
_data.writeString(characteristicUUID);
_data.writeString(value);
mRemote.transact(Stub.TRANSACTION_writeCharacteristicString, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void writeCharacteristicByteArray(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, byte[] value) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
_data.writeString(serviceUUID);
_data.writeString(characteristicUUID);
_data.writeByteArray(value);
mRemote.transact(Stub.TRANSACTION_writeCharacteristicByteArray, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void writeCharacteristicInt(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, int value, int format, int offset) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
_data.writeString(serviceUUID);
_data.writeString(characteristicUUID);
_data.writeInt(value);
_data.writeInt(format);
_data.writeInt(offset);
mRemote.transact(Stub.TRANSACTION_writeCharacteristicInt, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void writeDescriptor(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, java.lang.String descriptorUUID, byte[] value) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
_data.writeString(serviceUUID);
_data.writeString(characteristicUUID);
_data.writeString(descriptorUUID);
_data.writeByteArray(value);
mRemote.transact(Stub.TRANSACTION_writeDescriptor, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void readDescriptor(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, java.lang.String descriptorUUID) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
_data.writeString(serviceUUID);
_data.writeString(characteristicUUID);
_data.writeString(descriptorUUID);
mRemote.transact(Stub.TRANSACTION_readDescriptor, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public boolean isRetrying(java.lang.String deviceAddress) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
mRemote.transact(Stub.TRANSACTION_isRetrying, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public int getConnectionState(java.lang.String deviceAddress) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
mRemote.transact(Stub.TRANSACTION_getConnectionState, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public void readRemoteRSSI(java.lang.String deviceAddress) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeString(deviceAddress);
mRemote.transact(Stub.TRANSACTION_readRemoteRSSI, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
}
static final int TRANSACTION_scanDevices = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_startDeviceScan = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_stopDeviceScan = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_isScanning = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_connectDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_disconnectDevice = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_discoverServices = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_getServices = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
static final int TRANSACTION_getCharacteristics = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
static final int TRANSACTION_setCharacteristicNotification = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
static final int TRANSACTION_readCharacteristic = (android.os.IBinder.FIRST_CALL_TRANSACTION + 10);
static final int TRANSACTION_writeCharacteristicString = (android.os.IBinder.FIRST_CALL_TRANSACTION + 11);
static final int TRANSACTION_writeCharacteristicByteArray = (android.os.IBinder.FIRST_CALL_TRANSACTION + 12);
static final int TRANSACTION_writeCharacteristicInt = (android.os.IBinder.FIRST_CALL_TRANSACTION + 13);
static final int TRANSACTION_writeDescriptor = (android.os.IBinder.FIRST_CALL_TRANSACTION + 14);
static final int TRANSACTION_readDescriptor = (android.os.IBinder.FIRST_CALL_TRANSACTION + 15);
static final int TRANSACTION_isRetrying = (android.os.IBinder.FIRST_CALL_TRANSACTION + 16);
static final int TRANSACTION_getConnectionState = (android.os.IBinder.FIRST_CALL_TRANSACTION + 17);
static final int TRANSACTION_readRemoteRSSI = (android.os.IBinder.FIRST_CALL_TRANSACTION + 18);
}
public void scanDevices(java.lang.String[] advertisedServiceUUIDList, int periodMsec) throws android.os.RemoteException;
public void startDeviceScan() throws android.os.RemoteException;
public void stopDeviceScan() throws android.os.RemoteException;
public boolean isScanning() throws android.os.RemoteException;
public void connectDevice(java.lang.String deviceAddress, long timeoutMsec) throws android.os.RemoteException;
public void disconnectDevice(java.lang.String deviceAddress) throws android.os.RemoteException;
public void discoverServices(java.lang.String deviceAddress) throws android.os.RemoteException;
public void getServices(java.lang.String deviceAddress) throws android.os.RemoteException;
public void getCharacteristics(java.lang.String deviceAddress, java.lang.String serviceUUID) throws android.os.RemoteException;
public void setCharacteristicNotification(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, boolean enabled) throws android.os.RemoteException;
public void readCharacteristic(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID) throws android.os.RemoteException;
public void writeCharacteristicString(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, java.lang.String value) throws android.os.RemoteException;
public void writeCharacteristicByteArray(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, byte[] value) throws android.os.RemoteException;
public void writeCharacteristicInt(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, int value, int format, int offset) throws android.os.RemoteException;
public void writeDescriptor(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, java.lang.String descriptorUUID, byte[] value) throws android.os.RemoteException;
public void readDescriptor(java.lang.String deviceAddress, java.lang.String serviceUUID, java.lang.String characteristicUUID, java.lang.String descriptorUUID) throws android.os.RemoteException;
public boolean isRetrying(java.lang.String deviceAddress) throws android.os.RemoteException;
public int getConnectionState(java.lang.String deviceAddress) throws android.os.RemoteException;
public void readRemoteRSSI(java.lang.String deviceAddress) throws android.os.RemoteException;
}
