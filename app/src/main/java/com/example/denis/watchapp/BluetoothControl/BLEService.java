/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.denis.watchapp.BluetoothControl;


import android.annotation.TargetApi;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.Build;
import android.os.RemoteException;
import android.os.IBinder;
import android.util.Log;


import com.example.denis.watchapp.BluetoothControl.Command;
import com.example.denis.watchapp.BluetoothControl.TypeCommand;
import com.ratio.deviceService.DeviceErrorCodes;
import com.ratio.deviceService.DeviceService;
import com.ratio.exceptions.DeviceManagerException;
import com.ratio.exceptions.DeviceNameNotFoundException;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.List;


@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class BLEService extends DeviceService {
    //private final static String TAG = BluetoothLeService.class.getSimpleName();

    private BLEServiceCallback mBLEServiceCb = null;
    private boolean TimeSync = false;
    private TypeCommand commandWrite;
    private BluetoothGattCharacteristic mNotifyCharacteristic;
    private BluetoothGattService mNotifyService;
    public DeviceBluetoothControl mDeviceControl;
    static byte[] array = new byte[0];
    private Object callbaclLock = new Object();
    public Boolean isWriting=false;
    public boolean first;
    public int lenght;
    private static boolean readExtendData;
    public String mMac;
    public int mTimeOutCon = 35000;
    public TypeDevice typeDevice;
    public static String tag = "user";
    Object stoppedSync = new Object();
    private void displayCharacteristic(final BluetoothGattCharacteristic characteristic) {
        String msg = null;
        Log.d(tag, "чтение данных");
        byte[] data = null;
        data = characteristic.getValue();
        if (TimeSync) {
            Log.d(tag, "отправка команды");
            mDeviceControl.WriteCommand(commandWrite);


        } else {
            if (first) {
                ByteBuffer bb = ByteBuffer.wrap(data);
                bb.order(ByteOrder.BIG_ENDIAN);
                lenght = bb.getShort(2);
                first = false;
                if (data[1] == 0x04) {
                    readExtendData = true;
                }
            }
            if (!readExtendData) {
                array = new byte[0];
                Command command = new Command(data, typeDevice);
                synchronized (callbaclLock) {
                    if (mBLEServiceCb != null) {
                        mBLEServiceCb.displayCommand(command);
                    }
                }
            } else {
                byte[] arrayTemp = new byte[array.length + data.length];
                System.arraycopy(array, 0, arrayTemp, 0, array.length);
                System.arraycopy(data, 0, arrayTemp, array.length, data.length);
                array = arrayTemp;
                if (array.length == lenght && readExtendData) {
                    readExtendData = false;
                    System.arraycopy(array, 0, arrayTemp, 0, array.length);
                    Command command = new Command(array, typeDevice);
                    command.typeCommand = TypeCommand.ReadExtend;
                    synchronized (callbaclLock) {
                        if (mBLEServiceCb != null) {
                            mBLEServiceCb.displayCommand(command);
                        }
                    }
                    array = new byte[0];
                }
            }
            /*if (data.length == 18) {
                if (data[4] == 0x4f && data[5] == 0x4b) {
                    if (mBLEServiceCb != null) {
                        mBLEServiceCb.displayData(msg, data);
                    }

                } else {
                    if (mBLEServiceCb != null) {
                        mBLEServiceCb.displayData(msg, data);
                    }
                }
            } else {
                if (mBLEServiceCb != null) {
                    mBLEServiceCb.displayData(msg, data);
                }
            }*/
        }
    }

    @Override
    public void onDiscoveryStopped()  {
        try {
            Log.d(tag,"Поиск закончен");
            if (mBLEServiceCb != null) {
                mDeviceManager.connect(mMac, mTimeOutCon);
            }
            else
            {
                Log.d(tag,"сервис отключен ");
            }
        } catch (DeviceNameNotFoundException e) {
            Log.d(tag,"Не найден");
            synchronized (callbaclLock) {
                if (mBLEServiceCb != null) {
                    mBLEServiceCb.Error(DeviceErrorCodes.ERROR_DEVICE_NOT_FOUND);
                }
            }

        } catch (DeviceManagerException e) {
            Log.d(tag,"Что то нее хорошее");
            e.printStackTrace();
        }
    }

    @Override
    public void onGattConnectionState(BluetoothDevice device, BluetoothGatt gatt, int connState)  {
       if(connState ==BluetoothProfile.STATE_CONNECTED)
       {
           try {
               Log.d(tag,"Подключен. Поиск сервисов");
               mDeviceControl.discoverServices(mMac);
           } catch (RemoteException e) {
               e.printStackTrace();
           }
       }
    }

    @Override
    public void onServicesDiscovered(BluetoothDevice device, BluetoothGatt gatt) {
        Log.d(tag,"Выбор сервиса");
        if(mDeviceControl!=null) {
            List<BluetoothGattService> gattServices = null;
            try {
                gattServices = mDeviceControl.getGattService();
            } catch (DeviceNameNotFoundException e) {
                e.printStackTrace();
            }
            if (gattServices != null) {
                for (BluetoothGattService gattService : gattServices) {
                    List<BluetoothGattCharacteristic> gattCharacteristics =
                            gattService.getCharacteristics();
                    for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                        HashMap<String, String> currentCharaData = new HashMap<String, String>();
                        int uuidd = gattCharacteristic.getUuid().hashCode();
                        if (uuidd == 1813384059) {
                            final int charaProp = gattCharacteristic.getProperties();
                            if ((charaProp & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {

                                try {
                                    mDeviceControl.setCharacteristicNotification(device.getAddress(),gattService.getUuid().toString(),gattCharacteristic.getUuid().toString(),true);
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }
                                mNotifyCharacteristic = gattCharacteristic;
                                mNotifyService = gattService;
                                synchronized (callbaclLock) {
                                    if (mBLEServiceCb != null) {
                                        mBLEServiceCb.notifyConnectedGATT();
                                    }
                                }
                            }


                        }
                    }
                }

            }
        }
    }
    @Override
    public void onCharacteristicChanged(BluetoothDevice device, BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        displayCharacteristic(characteristic);
    }


    @Override
    protected void broadcastError(int errorCode, final String error, String deviceAddress) {
        synchronized (callbaclLock) {
            if (mBLEServiceCb != null) {
                mBLEServiceCb.Error(errorCode);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        if (!mfInitialized) {
            initialize();
            mfInitialized = true;
        }
        mDeviceControl = new DeviceBluetoothControl();
        return mDeviceControl;
    }

    public interface BLEServiceCallback {
        public void displayCommand(Command comand);

        public void notifyConnectedGATT();

        public void Error(int errorCode);
    }


    public class DeviceBluetoothControl extends DeviceCommandImpl {

        public void WriteCommand(final TypeCommand typeCommand) {

            if (mNotifyService != null && mNotifyCharacteristic != null)
            array = new byte[0];
                if (!TimeSync) {
                    commandWrite = typeCommand;
                    TimeSync = true;
                    Log.d(tag, "синхронизация");
                    try {
                        mDeviceManager.writeCharacteristic(mMac,mNotifyService.getUuid(), mNotifyCharacteristic.getUuid(), Command.GetComand(TypeCommand.TimeSync));
                    } catch (DeviceNameNotFoundException e) {
                        e.printStackTrace();
                    } catch (DeviceManagerException e) {
                        e.printStackTrace();
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                } else {
                    Log.d(tag, "запись команды");
                    TimeSync = false;
                    try {
                        first = true;
                        mDeviceManager.writeCharacteristic(mMac, mNotifyService.getUuid(), mNotifyCharacteristic.getUuid(), Command.GetComand(typeCommand));
                    } catch (DeviceNameNotFoundException e) {
                        e.printStackTrace();
                    } catch (DeviceManagerException e) {
                        e.printStackTrace();
                    }
                    catch (Exception ex)
                    {
                        ex.printStackTrace();
                    }
                }
            /*} else {
                mBLEServiceCb.isBusy();
            }*/
        }
        public List<BluetoothGattService> getGattService() throws DeviceNameNotFoundException {
           return  mDeviceManager.getSupportedGattServices(mMac);
        }

        public void setBLEServiceCb(BLEServiceCallback cb) {
            synchronized (callbaclLock) {
                Log.d(tag, "установка каллбека");
                mBLEServiceCb = cb;
            }
        }

        public void connectToModule(final String mac, final TypeDevice TypeDevice, final boolean retry)
        {
            Thread myThread = new Thread(new Runnable() {
                public void run()
                {
                    synchronized (stoppedSync){
                    try {
                        typeDevice = TypeDevice;
                        mMac = mac;
                        Log.d(tag, "Проверяем состояние");
                        int conState = mDeviceManager.getConnectionState(mac);
                        if (conState != 2) {
                            try {
                                Log.d(tag, "Есть но не подключенны");
                                if(retry)
                                {
                                    startDeviceScan();
                                }
                                else
                                {
                                    mDeviceManager.connect(mac, mTimeOutCon);
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    } catch (Exception ex) {
                        try {
                            Log.d(tag, "Не найден, ищем");
                            startDeviceScan();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
                }
            });

            myThread.start();


        }

        public String getMac()
        {
            return mMac;
        }
        public int getState() {
            try {
                int conState = mDeviceManager.getConnectionState(mMac);
                return conState;
            } catch (DeviceNameNotFoundException e) {
                e.printStackTrace();
                return BluetoothProfile.STATE_DISCONNECTED;
            }
        }

        @Override
        public void disconnectDevice(String deviceAddress) throws RemoteException {
            synchronized (stoppedSync) {
                try {
                    mDeviceManager.disconnect(deviceAddress);
                } catch (Exception ex) {
                  //  broadcastError(DeviceErrorCodes.ERROR_DISCONNECT, ex.getMessage(), deviceAddress);
                }
                mDeviceManager.stopLeScan();
            }
        }
    }
}
