package com.example.denis.watchapp;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothProfile;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.denis.watchapp.BluetoothControl.BLEService;
import com.example.denis.watchapp.BluetoothControl.Command;
import com.example.denis.watchapp.BluetoothControl.StateSD;
import com.example.denis.watchapp.BluetoothControl.TypeCommand;
import com.example.denis.watchapp.BluetoothControl.TypeDevice;
import com.ratio.deviceService.DeviceErrorCodes;

import java.util.ArrayDeque;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by denis on 20.09.2014.
 */
public  class FragmentMI extends android.support.v4.app.Fragment implements MyActivity.FragmentBluetoothModule {
    private BLEService.DeviceBluetoothControl mBluetoothLeService;
    public static ProgressDialog dialog;
    public TextView txtPower, txtResult, txtCharge,txtSKZ;
    public static Switch toggle;
    public Button btnMarker;
    public int connectNumber;
    int CONN_COUNT = 3;
    public int connectCount = CONN_COUNT;

    public ArrayDeque<TypeCommand> commands;
    Timer myTimer;
    int TimeElapse= 2000;
    String tag="user";
    private final Object obj = new Object();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.control_mi, container, false);
        mDCServiceCb = new DCServiceCb();
        txtSKZ = (TextView) rootView.findViewById(R.id.txtSKZ);
        txtPower = (TextView) rootView.findViewById(R.id.txtPower);
        txtCharge = (TextView) rootView.findViewById(R.id.txtCharge);
        txtResult = (TextView) rootView.findViewById(R.id.resultTxt);
        ((TextView) rootView.findViewById(R.id.txtDPower)).setText("Миограмма:");
        commands = new ArrayDeque<TypeCommand>();
        connectNumber = 3;
        btnMarker = (Button) rootView.findViewById(R.id.btnMarker);
        btnMarker.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                synchronized (obj) {
                    commands.add(TypeCommand.Marker);
                    GetBluetoothService().WriteCommand(TypeCommand.Marker);
                }
            }
        });
        toggle = (Switch) rootView.findViewById(R.id.swithSd);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    synchronized (obj) {
                        commands.add(TypeCommand.Run);
                        GetBluetoothService().WriteCommand(TypeCommand.Run);
                    }
                } else {
                    synchronized (obj) {
                        commands.add(TypeCommand.Stop);
                        GetBluetoothService().WriteCommand(TypeCommand.Stop);
                    }
                }
            }
        });
        return rootView;
    }

    public void StopTimer()
    {
        if (myTimer != null) {
            myTimer.cancel();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        connectCount = 3;
        if (GetBluetoothService() != null) {
            onConnectAction();
        }
    }

    @Override
    public void onReloadAction() {
        synchronized (obj) {
            commands.add(TypeCommand.ReadStandart);
            GetBluetoothService().WriteCommand(TypeCommand.ReadStandart);
        }
    }

    @Override
    public void onTimer() {
        StopTimer() ;
        myTimer = new Timer();
        myTimer.schedule(new TimerTask() { // Определяем задачу
            @Override
            public void run() {
                synchronized (obj)
                {
                    Log.d(tag, "автоматический запрос  ControlSectionFragment");
                    commands.add(TypeCommand.ReadStandart);
                    GetBluetoothService().WriteCommand(TypeCommand.ReadStandart);
                }
            }
        }, 0L, TimeElapse);
    }

    @Override
    public void onConnectAction() {
        if (GetBluetoothService().getState() != BluetoothProfile.STATE_CONNECTED ) {
            GetBluetoothService().setBLEServiceCb(mDCServiceCb);
            GetBluetoothService().connectToModule(GetMac(), TypeDevice.MI,false);
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Подключение");
            dialog.setCancelable(true);
            dialog.show();
        } else {
            Toast.makeText(getActivity(), "Вы уже подключены", Toast.LENGTH_SHORT).show();
        }


    }

    public class DCServiceCb implements BLEService.BLEServiceCallback {


        @Override
        public void displayCommand(final Command command) {
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(commands.isEmpty())
                        {
                            Log.d(tag, "ошибка стр  Fragment MI   модуля");
                            return;
                        }
                        TypeCommand prevCommand = commands.pop();
                        if (command.typeCommand == TypeCommand.OK) {
                            if(prevCommand==TypeCommand.Marker)
                            {
                                Toast.makeText(getActivity(), "Ок", Toast.LENGTH_SHORT).show();
                            }
                            if (prevCommand == TypeCommand.Run) {
                                txtResult.setText("Идет запись");
                            }
                            if (prevCommand == TypeCommand.Stop) {
                                txtResult.setText("Запись выключена");
                            }
                        }
                        if (command.typeCommand == TypeCommand.Error) {
                            //Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                            if (prevCommand == TypeCommand.Run) {
                                toggle.setChecked(true);
                            }
                            if(prevCommand==TypeCommand.Stop)
                            {
                                toggle.setChecked(false);
                            }
                        }

                        if (command.typeCommand == TypeCommand.ReadStandart) {
                            txtPower.setText(String.valueOf(command.Power) +" мкВ");
                            txtSKZ.setText(String.valueOf(command.SKZ2) +" мкВ");
                            txtCharge.setText(String.valueOf(command.Charge)+"%");
                            if(command.SD == StateSD.Work)
                            {
                                txtResult.setText("Идет запись");
                                toggle.setChecked(true);
                            }
                            if(command.SD == StateSD.Stopped)
                            {
                                txtResult.setText("Запись выключена");
                                toggle.setChecked(false);
                            }
                        }
                    }
                });
        }

        @Override
        public void notifyConnectedGATT() {
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        toggle.setEnabled(true);
                        Toast.makeText(getActivity(), "Подключено", Toast.LENGTH_SHORT).show();
                        commands.add(TypeCommand.ReadStandart);
                        GetBluetoothService().WriteCommand(TypeCommand.ReadStandart);

                    }
                });


        }

        @Override
        public void Error(final int errorCode) {
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (connectCount > 0) {
                            connectCount--;
                            Log.d("Bluetooth:", "Повторная Попытка номер " + String.valueOf(CONN_COUNT - connectCount));
                            GetBluetoothService().connectToModule(GetMac(), TypeDevice.BM, true);
                        }
                        else {
                            if (errorCode == DeviceErrorCodes.ERROR_DEVICE_NOT_FOUND) {
                                dialog.dismiss();
                                Toast.makeText(getActivity(), "Невозможно подключится к устройству", Toast.LENGTH_LONG).show();
                            }
                            if (errorCode == DeviceErrorCodes.ERROR_CONNECTION_TIMEOUT) {
                                if (GetBluetoothService().getState() != BluetoothProfile.STATE_CONNECTED) {
                                    dialog.dismiss();
                                    Toast.makeText(getActivity(), "Истек таймаут подключения к устройству", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }

                });
        }


    }

    public static BLEService.BLEServiceCallback mDCServiceCb;

    public BLEService.DeviceBluetoothControl GetBluetoothService() {
        if (mBluetoothLeService == null) {
            mBluetoothLeService = ((MyActivity) getActivity()).mBluetoothLeService;

        }

        return mBluetoothLeService;
    }

    public String GetMac() {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        return SP.getString("MAC_MI", "NA");
    }

    public static FragmentMI newInstance(int sectionNumber) {
        FragmentMI fragment = new FragmentMI();
        return fragment;
    }

    public void onStop() {
        super.onStop();
        connectCount=0;
        StopTimer();
        if (GetBluetoothService() != null) {
            GetBluetoothService().setBLEServiceCb(null);
            try {

                GetBluetoothService().disconnectDevice(GetMac());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        if(dialog!=null)
        {
            dialog.dismiss();
        }
    }
}
