package com.example.denis.watchapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.viewpagerindicator.TabPageIndicator;

public  class PreferenceFragment extends android.support.v4.app.Fragment implements MyActivity.FragmentBluetoothModule {
    CollectionPagerAdapter mDemoCollectionPagerAdapter;
    ViewPager mViewPager;
    String tag = "USER";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(tag, "создание  PreferenceFragment   модуля");
        View rootView = inflater.inflate(R.layout.preference_layout, container, false);
        mDemoCollectionPagerAdapter =
                new CollectionPagerAdapter(
                        getChildFragmentManager());
        mViewPager = (ViewPager) rootView.findViewById(R.id.pagerHorizontal);
        mViewPager.setAdapter(mDemoCollectionPagerAdapter);
        //Bind the title indicator to the adapter
        TabPageIndicator titleIndicator = (TabPageIndicator) rootView.findViewById(R.id.tabPageIndicator);
        titleIndicator.setViewPager(mViewPager);
        //  if(getActivity()!=null)
        {
            //        ((MyActivity)getActivity()).mBluetoothAdapter.disable();
            //       ((MyActivity)getActivity()).mBluetoothAdapter.enable();
        }
        return rootView;
    }


    @Override
    public void onReloadAction() {
        int k= 7/0;
    }

    @Override
    public void onTimer() {

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(tag, "остановка  PreferenceFragment   модуля");
    }
    @Override
    public void onConnectAction() {
        int k= 7/0;
    }

    @Override
    public void onStop() {
        super.onStop();

    }
    public class CollectionPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

        public CollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new PreferenceModuleFragment();
            Bundle args = new Bundle();
            switch (i) {
                case 0: {
                    args.putString(PreferenceModuleFragment.NAME_MODULE, "BM");
                    break;
                }
                case 1: {
                    args.putString(PreferenceModuleFragment.NAME_MODULE, "IN");
                    break;
                }
                case 2: {
                    args.putString(PreferenceModuleFragment.NAME_MODULE, "MI");
                    break;
                }
            }
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String nameTab = "";
            switch (position) {
                case 0: {
                    nameTab = "Базовый модуль";
                    break;
                }
                case 1: {
                    nameTab = "Инерциальный модуль";
                    break;
                }
                case 2: {
                    nameTab = "Миографический";
                    break;
                }
            }
            return nameTab;
        }
    }

    // Instances of this class are fragments representing a single
// object in our collection.
    public static class PreferenceModuleFragment extends Fragment {
        public static final String NAME_MODULE = "NameModule";
        private TextView txtName, txtMac;
        private String name, mac, nameModule;

        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(
                    R.layout.preference_fragment_module_bm, container, false);
            txtName = (TextView) rootView.findViewById(R.id.txtName);
            txtMac = (TextView) rootView.findViewById(R.id.txtMac);
            Bundle args = getArguments();
            nameModule = args.getString(NAME_MODULE);
            Button btnSelect = (Button) rootView.findViewById(R.id.btnSelect);
            btnSelect.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), DeviceScanActivity.class);
                    intent.putExtra(DeviceScanActivity.NAME_MODULE, nameModule);
                    startActivity(intent);
                }
            });
            return rootView;
        }

        @Override
        public void onResume() {
            super.onResume();
            SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            mac = SP.getString("MAC_" + nameModule, "NA");
            name = SP.getString("NAME_" + nameModule, "NA");
            txtMac.setText(mac);
            txtName.setText(name);
        }


    }

}
