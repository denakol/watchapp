package com.example.denis.watchapp.BluetoothControl;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.ratio.deviceService.DeviceErrorCodes;
import com.ratio.deviceService.DeviceService;
import com.ratio.exceptions.DeviceManagerException;
import com.ratio.exceptions.DeviceNameNotFoundException;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.List;

/**
 * Created by denis on 24.09.2014.
 */
public class MultipleBLEService extends DeviceService {

    private BLEServiceCallback mBLEServiceCb = null;
    public DeviceBluetoothControl mDeviceControl;
    private Object callbaclLock = new Object();
    public String mMac;
    public int mTimeOutCon = 35000;
    public TypeDevice typeDevice;
    public static String tag = "user";
    Object stoppedSync = new Object();
    public String _macBM;
    public String _macIN;
    private Boolean _scanning=false;
    private Object lockQueue = new Object();
    private ArrayDeque<BluetoothModule> modules;
    @Override
    public void onDiscoveryStopped()  {
        try {
            _scanning=false;
            Log.d(tag, "Поиск закончен");
            if (mBLEServiceCb != null) {
                Connect(_macBM);
                Connect(_macIN);
            }
            else
            {
                Log.d(tag,"сервис отключен ");
            }
        } catch (DeviceNameNotFoundException e) {
            Log.d(tag,"Не найден");
            synchronized (callbaclLock) {
                if (mBLEServiceCb != null) {
                    mBLEServiceCb.Error(DeviceErrorCodes.ERROR_DEVICE_NOT_FOUND);
                }
            }

        }
    }

    @Override
    public void onGattConnectionState(BluetoothDevice device, BluetoothGatt gatt, int connState)  {
        if(connState == BluetoothProfile.STATE_CONNECTED)
        {
            try {
                Log.d(tag,"Подключен. Поиск сервисов"+device.getAddress());
                BluetoothModule module =new BluetoothModule(device.getAddress());
                if(device.getAddress().equals(_macBM))
                {
                    module.typeDevice = TypeDevice.BM;
                }
                else
                {
                    module.typeDevice = TypeDevice.IN;
                }
                synchronized (lockQueue)
                {
                    modules.push(module);
                }
                mDeviceControl.discoverServices(device.getAddress());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothDevice device, BluetoothGatt gatt) {
       BluetoothModule module = getBluetoothModules(device.getAddress());

        Log.d(tag,"Выбор сервиса");
        if(mDeviceControl!=null) {
            List<BluetoothGattService> gattServices = null;
            try {
                gattServices = mDeviceControl.getGattService(device.getAddress());
            } catch (DeviceNameNotFoundException e) {
                e.printStackTrace();
            }
            if (gattServices != null) {
                for (BluetoothGattService gattService : gattServices) {
                    List<BluetoothGattCharacteristic> gattCharacteristics =
                            gattService.getCharacteristics();
                    for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                        HashMap<String, String> currentCharaData = new HashMap<String, String>();
                        int uuidd = gattCharacteristic.getUuid().hashCode();
                        if (uuidd == 1813384059) {
                            final int charaProp = gattCharacteristic.getProperties();
                            if ((charaProp & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {

                                try {
                                    mDeviceControl.setCharacteristicNotification(device.getAddress(),gattService.getUuid().toString(),gattCharacteristic.getUuid().toString(),true);
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }
                                module.mNotifyCharacteristic = gattCharacteristic;
                                module.mNotifyService = gattService;
                                synchronized (callbaclLock) {
                                    if (mBLEServiceCb != null) {
                                        mBLEServiceCb.notifyConnectedGATT(module.typeDevice);
                                    }
                                }
                            }


                        }
                    }
                }

            }
        }
    }
    @Override
    public void onCharacteristicChanged(BluetoothDevice device, BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        BluetoothModule module = getBluetoothModules(device.getAddress());
        Log.d(tag, "чтение данных");
        byte[] data = null;
        data = characteristic.getValue();
        if (module.TimeSync) {
            Log.d(tag, "отправка команды");
            mDeviceControl.WriteCommand(module.commandWrite,module.typeDevice);
        } else {
            if (module.first) {
                ByteBuffer bb = ByteBuffer.wrap(data);
                bb.order(ByteOrder.BIG_ENDIAN);
                module.lenght = bb.getShort(2);
                module.first = false;
                if (data[1] == 0x04) {

                    module.readExtendData = true;
                }
            }
            if (!module.readExtendData) {
                module.array = new byte[0];
                Command command = new Command(data, module.typeDevice);
                synchronized (callbaclLock) {
                    if (mBLEServiceCb != null) {
                        mBLEServiceCb.displayCommand(command,module.typeDevice);
                    }
                }
            } else {
                byte[] arrayTemp = new byte[module.array.length + data.length];
                System.arraycopy(module.array, 0, arrayTemp, 0, module.array.length);
                System.arraycopy(data, 0, arrayTemp, module.array.length, data.length);
                module.array = arrayTemp;
                if (module.array.length == module.lenght && module.readExtendData) {
                    module.readExtendData = false;
                    System.arraycopy(module.array, 0, arrayTemp, 0, module.array.length);
                    Command command = new Command(module.array, typeDevice);
                    command.typeCommand = TypeCommand.ReadExtend;
                    synchronized (callbaclLock) {
                        if (mBLEServiceCb != null) {
                            mBLEServiceCb.displayCommand(command,module.typeDevice);
                        }
                    }
                    module.array = new byte[0];
                }
            }
            /*if (data.length == 18) {
                if (data[4] == 0x4f && data[5] == 0x4b) {
                    if (mBLEServiceCb != null) {
                        mBLEServiceCb.displayData(msg, data);
                    }

                } else {
                    if (mBLEServiceCb != null) {
                        mBLEServiceCb.displayData(msg, data);
                    }
                }
            } else {
                if (mBLEServiceCb != null) {
                    mBLEServiceCb.displayData(msg, data);
                }
            }*/
        }
    }


    @Override
    protected void broadcastError(int errorCode, final String error, String deviceAddress) {
        synchronized (callbaclLock) {
            if (mBLEServiceCb != null) {
                mBLEServiceCb.Error(errorCode);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        if (!mfInitialized) {
            initialize();
            modules = new ArrayDeque<BluetoothModule>();
            mfInitialized = true;
        }
        mDeviceControl = new DeviceBluetoothControl();
        return mDeviceControl;
    }

    public interface BLEServiceCallback {
        public void displayCommand(Command comand,TypeDevice typeDevice);

        public void notifyConnectedGATT(TypeDevice typeDevice);

        public void Error(int errorCode);
    }

    public class DeviceBluetoothControl extends DeviceCommandImpl {

        public void WriteCommand(final TypeCommand typeCommand, final TypeDevice typeDevice) {

            BluetoothModule module = getBluetoothModules(typeDevice);
            if(module==null)
            {
                Log.d(tag, "Запись команды устройство отсутствует");
                return;
            }
            if (module.mNotifyCharacteristic != null) {
                module.array = new byte[0];
                if (!module.TimeSync) {
                    module.commandWrite = typeCommand;
                    module.TimeSync = true;
                    Log.d(tag, "синхронизация");
                    try {
                        mDeviceManager.writeCharacteristic(module.Mac, module.mNotifyService.getUuid(), module.mNotifyCharacteristic.getUuid(), Command.GetComand(TypeCommand.TimeSync));
                    } catch (DeviceNameNotFoundException e) {
                        e.printStackTrace();
                    } catch (DeviceManagerException e) {
                        e.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    Log.d(tag, "запись команды");
                    module.TimeSync = false;
                    try {
                        module.first = true;
                        mDeviceManager.writeCharacteristic(module.Mac, module.mNotifyService.getUuid(), module.mNotifyCharacteristic.getUuid(), Command.GetComand(typeCommand));
                    } catch (DeviceNameNotFoundException e) {
                        e.printStackTrace();
                    } catch (DeviceManagerException e) {
                        e.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            /*} else {
                mBLEServiceCb.isBusy();
            }*/
            }
        }
        public List<BluetoothGattService> getGattService(String mac) throws DeviceNameNotFoundException {
            return  mDeviceManager.getSupportedGattServices(mac);
        }

        public void setBLEServiceCb(BLEServiceCallback cb) {
            synchronized (callbaclLock) {
                Log.d(tag, "установка каллбека");
                mBLEServiceCb = cb;
            }
        }

        public void connectToModule(final String mac, final TypeDevice TypeDevice, final boolean retry)
        {
            Thread myThread = new Thread(new Runnable() {
                public void run()
                {

                        try {
                            typeDevice = TypeDevice;
                            mMac = mac;
                            Log.d(tag, "Проверяем состояние");
                            int conState = mDeviceManager.getConnectionState(mac);
                            if (conState != 2) {
                                try {
                                    Log.d(tag, "Есть но не подключенны");
                                    if(retry)
                                    {
                                        startDeviceScan();
                                    }
                                    else
                                    {
                                        mDeviceManager.connect(mac, mTimeOutCon);
                                    }

                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        } catch (Exception ex) {
                            try {
                                Log.d(tag, "Не найден, ищем");
                                startDeviceScan();
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                    }

            });

            myThread.start();


        }
        public void connectToModule(final String macBM, final String macIN, final boolean retryTemp)
        {
            Thread myThread = new Thread(new Runnable() {
                public void run()
                {
                    boolean retry = retryTemp;
                    _macIN= macIN;
                    _macBM = macBM;
                    try {
                        Log.d(tag, "Проверяем состояние");
                        if(!retry) {
                            try {
                                Connect(macBM);

                            } catch (Exception ex) {
                                retry = true;
                            }
                        }
                        if(!retry) {
                            try {

                                Connect(macIN);
                            } catch (Exception ex) {
                                retry = true;
                            }
                        }
                        if(retry)
                        {

                            if(!_scanning) {
                                _scanning=true;
                                startDeviceScan();
                            }
                        }
                    } catch (Exception ex) {
                        try {
                            Log.d(tag, "Не найден, ищем");
                            _macBM = macBM;
                            _macIN= macIN;
                            if(!_scanning) {
                                _scanning=true;
                                startDeviceScan();
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }

            });

            myThread.start();


        }

        public String getMac()
        {
            return mMac;
        }
        public int getState() {
            try {
                int conState = mDeviceManager.getConnectionState(mMac);
                return conState;
            } catch (DeviceNameNotFoundException e) {
                e.printStackTrace();
                return BluetoothProfile.STATE_DISCONNECTED;
            }
        }

        @Override
        public void disconnectDevice(String deviceAddress) throws RemoteException {
            synchronized (stoppedSync) {
                try {
                    mDeviceManager.disconnect(deviceAddress);
                    modules = new ArrayDeque<BluetoothModule>();
                } catch (Exception ex) {
                    //  broadcastError(DeviceErrorCodes.ERROR_DISCONNECT, ex.getMessage(), deviceAddress);
                }
                mDeviceManager.stopLeScan();
            }
        }
    }

    public void Connect(String macBM) throws DeviceNameNotFoundException {
        int conState = mDeviceManager.getConnectionState(macBM);
        if (conState != 2) {
            try {
                Log.d(tag, "Есть но не подключенны");
                mDeviceManager.connect(macBM, mTimeOutCon);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public BluetoothModule getBluetoothModules(String mac)
    {
        for (BluetoothModule deviceInfo : modules) {
            if (deviceInfo.Mac.equals(mac)) {
                return deviceInfo;

            }
        }
        return null;
    }
    public BluetoothModule getBluetoothModules(TypeDevice typeDevice)
    {
        for (BluetoothModule deviceInfo : modules) {
            if (deviceInfo.typeDevice==typeDevice) {
                return deviceInfo;

            }
        }
        return null;
    }
}
