package com.example.denis.watchapp;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothProfile;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.example.denis.watchapp.BluetoothControl.BLEService;
import com.example.denis.watchapp.BluetoothControl.TypeDevice;
import com.ratio.deviceService.DeviceErrorCodes;

/**
 * Created by denis on 21.09.2014.
 */
public  abstract class BluetoothControlFragment extends android.support.v4.app.Fragment implements FragmentBM.IBluetoothControl {
    int CONN_COUNT = 3;
    int TimeElapse = 4000;
    Boolean stopped;
    String tag = "user";
    public static ProgressDialog dialog;
    public int connectCount = CONN_COUNT;
    Object stoppedSync = new Object();
    public BLEService.DeviceBluetoothControl GetBluetoothService() {
        return ((MyActivity) getActivity()).mBluetoothLeService;
    }

    @Override
    public void onResume() {
        synchronized (stoppedSync) {

            super.onResume();
            Log.d(tag, "возобновление   модуля");
            connectCount = CONN_COUNT;
            stopped = false;
        }
    }

    public String GetMacBm() {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        return SP.getString("MAC_BM", "NA");
    }

    public abstract class BluetoothCallback implements BLEService.BLEServiceCallback {

        @Override
        public void Error(final int errorCode) {
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (stoppedSync) {
                            if(!stopped){
                            if (connectCount > 0) {
                                connectCount--;
                                Log.d("Bluetooth:", "Повторная Попытка номер " + String.valueOf(CONN_COUNT - connectCount));
                                GetBluetoothService().connectToModule(GetMacBm(), TypeDevice.BM, true);
                            } else {
                                if (errorCode == DeviceErrorCodes.ERROR_DEVICE_NOT_FOUND) {
                                    dialog.dismiss();
                                    Toast.makeText(getActivity(), "Устройство не обнаружено", Toast.LENGTH_LONG).show();
                                }
                                if (errorCode == DeviceErrorCodes.ERROR_CONNECTION_TIMEOUT) {

                                    if (GetBluetoothService().getState() != BluetoothProfile.STATE_CONNECTED) {
                                        dialog.dismiss();
                                        Toast.makeText(getActivity(), "Истек таймаут подключения к устройству", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        }
                        }
                    }
                });
        }
    }
}
