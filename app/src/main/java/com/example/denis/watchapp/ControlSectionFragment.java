package com.example.denis.watchapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothProfile;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.denis.watchapp.BluetoothControl.BLEService;
import com.example.denis.watchapp.BluetoothControl.Command;
import com.example.denis.watchapp.BluetoothControl.StateSD;
import com.example.denis.watchapp.BluetoothControl.TypeCommand;

import java.util.ArrayDeque;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by denis on 21.09.2014.
 */
public  class ControlSectionFragment extends BluetoothControlFragment {
    public Switch toggle;
    public ArrayDeque<TypeCommand> commands;
    public MyActivity mActivity;
    public TextView txtHeartRate, txtResult, txtCharge;
    public Button btnMarker;
    Timer myTimer;
    private final Object obj = new Object();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(tag, "запуск  ControlSectionFragment");
        View rootView = inflater.inflate(R.layout.control_device_bm, container, false);
        mDCServiceCb = new DCServiceCb();
        commands = new ArrayDeque<TypeCommand>();
        toggle = (Switch) rootView.findViewById(R.id.swithSd);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.d(tag, "запись  ControlSectionFragment   модуля");
                if (isChecked) {
                    synchronized (obj) {
                        commands.add(TypeCommand.Run);
                        GetBluetoothService().WriteCommand(TypeCommand.Run);
                    }
                } else {
                    synchronized (obj) {
                        commands.add(TypeCommand.Stop);
                        GetBluetoothService().WriteCommand(TypeCommand.Stop);
                    }
                }
            }
        });
        btnMarker = (Button) rootView.findViewById(R.id.btnMarker);
        btnMarker.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                synchronized (obj) {
                    commands.add(TypeCommand.Marker);
                    GetBluetoothService().WriteCommand(TypeCommand.Marker);
                }
            }
        });
        Log.d("Fragment", "On createview control");
        txtHeartRate = (TextView) rootView.findViewById(R.id.txtHeartRate);
        txtResult = (TextView) rootView.findViewById(R.id.resultTxt);
        txtCharge = (TextView) rootView.findViewById(R.id.txtCharge);
        return rootView;
    }

    @Override
    public void reload() {
        Log.d(tag, "перезагрузка  ControlSectionFragment");
        onSelected();
    }

    @Override
    public void onSelected() {
        Log.d(tag, "выбор  ControlSectionFragment");
        if (GetBluetoothService() != null) {
            GetBluetoothService().setBLEServiceCb(mDCServiceCb);
            if (GetBluetoothService().getState() == BluetoothProfile.STATE_CONNECTED) {
                synchronized (obj) {
                    commands.add(TypeCommand.ReadStandart);
                    GetBluetoothService().WriteCommand(TypeCommand.ReadStandart);
                    toggle.setEnabled(true);
                }
            } else {
                toggle.setEnabled(false);
            }
        }
    }

    public class DCServiceCb extends BluetoothControlFragment.BluetoothCallback {

        @Override
        public void displayCommand(final Command command) {
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(tag, "просмотр команды  ControlSectionFragment   модуля");
                        if(command.typeCommand != TypeCommand.ReadExtend) {
                            if (commands.isEmpty()) {
                                Log.d(tag, "ошибка стр  ControlSectionFragment   модуля");
                                return;
                            }
                            TextView resultTxt = (TextView) (getActivity().findViewById(R.id.resultTxt));
                            TypeCommand prevCommand = commands.pop();
                            if (command.typeCommand == TypeCommand.OK) {

                                if (prevCommand == TypeCommand.Run) {
                                    resultTxt.setText("Идет запись");
                                }
                                if (prevCommand == TypeCommand.Stop) {
                                    resultTxt.setText("Запись выключена");
                                }
                                if (prevCommand == TypeCommand.Marker) {
                                    Toast.makeText(getActivity(), "Ок", Toast.LENGTH_SHORT).show();
                                }

                            }
                            if (command.typeCommand == TypeCommand.Error) {
                                //Toast.makeText(getActivity(), "Ошибка", Toast.LENGTH_SHORT).show();
                                if (prevCommand == TypeCommand.Run) {
                                    toggle.setChecked(true);
                                }
                                if (prevCommand == TypeCommand.Stop) {
                                    toggle.setChecked(false);
                                }
                            }

                            if (command.typeCommand == TypeCommand.ReadStandart) {
                                txtCharge.setText(String.valueOf(command.Charge) + "%");
                                txtHeartRate.setText(String.valueOf(command.HeartRate) + " уд/мин");
                                if (command.SD == StateSD.Work) {
                                    resultTxt.setText("Идет запись");
                                    toggle.setChecked(true);
                                }
                                if (command.SD == StateSD.Stopped) {
                                    resultTxt.setText("Запись выключена");
                                    toggle.setChecked(false);
                                }
                            }
                        }
                    }
                });
        }

        @Override
        public void notifyConnectedGATT() {
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toggle.setEnabled(true);
                        dialog.dismiss();
                        Toast.makeText(getActivity(), "Подключено", Toast.LENGTH_SHORT).show();
                        onSelected();
                    }
                });


        }
    }

    @Override
    public void resumeSelected() {
        Log.d(tag, "возобновление  ControlSectionFragment");
        if (GetBluetoothService() != null) {
            if (GetBluetoothService().getState() == BluetoothProfile.STATE_CONNECTED) {

            } else {
                dialog = new ProgressDialog(getActivity());
                dialog.setMessage("Подключение");
                dialog.setCancelable(true);
                dialog.show();
            }
            onSelected();
        }

    }

    @Override
    public void timer() {
        StopTimer() ;
        myTimer = new Timer();
        myTimer.schedule(new TimerTask() { // Определяем задачу
            @Override
            public void run() {
                synchronized (obj)
                {
                    Log.d(tag, "автоматический запрос  ControlSectionFragment");
                    commands.add(TypeCommand.ReadStandart);
                    GetBluetoothService().WriteCommand(TypeCommand.ReadStandart);
                }
            }
        }, 0L, TimeElapse);
    }
    @Override
    public void StopTimer()
    {
        if (myTimer != null) {
            myTimer.cancel();
        }
    }
    @Override
    public void onStop() {
        synchronized (stoppedSync) {
            super.onStop();
            StopTimer();
            connectCount = 0;
            stopped = true;
        }
    }
    public  BLEService.BLEServiceCallback mDCServiceCb;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MyActivity) activity;
    }
}

