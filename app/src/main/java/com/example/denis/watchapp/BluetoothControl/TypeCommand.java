package com.example.denis.watchapp.BluetoothControl;

/**
 * Created by denis on 21.09.2014.
 */
public enum TypeCommand {
    Enable,
    Disable,
    Run,
    Stop,
    TimeSync,
    ReadStandart,
    ReadExtend,
    OK,
    Error,
    Marker,
    ReadExtendSecond

}
