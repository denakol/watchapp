package com.example.denis.watchapp.BluetoothControl;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

/**
 * Created by denis on 24.09.2014.
 */
public class BluetoothModule {
    public String Mac;
    public boolean TimeSync = false;
    public TypeCommand commandWrite;
    static byte[] array = new byte[0];
    public boolean first;
    public int lenght;
    public static boolean readExtendData;
    public TypeDevice typeDevice;
    public static String tag = "user";
    public BluetoothGattCharacteristic mNotifyCharacteristic;
    public BluetoothGattService mNotifyService;
    public BluetoothModule(String mac)
    {
        Mac=mac;
    }
}
