package com.example.denis.watchapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.denis.watchapp.BluetoothControl.BLEService;
import com.example.denis.watchapp.BluetoothControl.TypeDevice;

/**
 * Created by denis on 20.09.2014.
 */
public  class FragmentBM extends FragmentModule implements MyActivity.FragmentBluetoothModule {
    private static final String ARG_SECTION_NUMBER = "section_number";
    private BLEService.DeviceBluetoothControl mBluetoothLeService;
    public static ProgressDialog dialog;
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;
    VerticalViewPager  mViewPager;
    static IBluetoothControl  currentPage;
    int CONN_COUNT = 3;
    int TimeElapse;
    String tag = "user";
    public static FragmentBM newInstance(int sectionNumber) {
        FragmentBM fragment = new FragmentBM();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }

    public FragmentBM() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(tag, "создание  FragmentBM   модуля");
        View rootView = inflater.inflate(R.layout.fragment_my, container, false);
        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getChildFragmentManager());
        mViewPager = (VerticalViewPager) rootView.findViewById(R.id.pager);
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                android.support.v4.app.Fragment page = (getChildFragmentManager()).findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
                if (currentPage != null)
                {
                    currentPage.StopTimer();
                }
                ((IBluetoothControl) page).onSelected();

                currentPage= ((IBluetoothControl) page);

            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        Log.d(tag, "восстановление  FragmentBM   модуля");
        super.onResume();
        if (GetBluetoothService() != null) {
            android.support.v4.app.Fragment page = (getChildFragmentManager()).findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
            if (page != null) {
                ((IBluetoothControl) page).resumeSelected();
            }
            GetBluetoothService().connectToModule(GetMacBm(), TypeDevice.BM,false);
        }
    }

    @Override
    public void onConnectAction() {
        Log.d(tag, "соединение  FragmentBM   модуля");
        android.support.v4.app.Fragment page = (getChildFragmentManager()).findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
        if (page != null)
            ((IBluetoothControl) page).resumeSelected();
        GetBluetoothService().connectToModule(GetMacBm(), TypeDevice.BM,false);
    }

    public String GetMacBm() {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        return SP.getString("MAC_BM", "NA");
    }

    public void onStop() {
        Log.d(tag, "остановка  FragmentBM   модуля");
        if(dialog!=null)
        {
            dialog.dismiss();
        }
        super.onStop();
        if (GetBluetoothService() != null) {
            GetBluetoothService().setBLEServiceCb(null);
            try {
                GetBluetoothService().disconnectDevice(GetMacBm());
            } catch (RemoteException e) {
               e.printStackTrace();
           }
        }
    }

    public BLEService.DeviceBluetoothControl GetBluetoothService() {
        if (mBluetoothLeService == null) {
            mBluetoothLeService = ((MyActivity) getActivity()).mBluetoothLeService;

        }

        return mBluetoothLeService;
    }

    @Override
    public void onReloadAction() {
        android.support.v4.app.Fragment page = getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
        ((IBluetoothControl) page).reload();
    }

    @Override
    public void onTimer() {
        android.support.v4.app.Fragment page = getChildFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + mViewPager.getCurrentItem());
        ((IBluetoothControl) page).timer();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MyActivity) activity).onSectionAttached(0);
    }

    public  class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        public AppSectionsPagerAdapter(android.support.v4.app.FragmentManager fm) {
            super(fm);

        }
        @Override
        public android.support.v4.app.Fragment getItem(int i) {
            android.support.v4.app.Fragment frag;
            switch (i) {
                case 0:
                    // The first section of the app is the most interesting -- it offers
                    // a launchpad into the other demonstrations in this example application.
                    frag = new GraphSectionFragment();
                    currentPage = (IBluetoothControl)frag;
                    break;

                case 1:
                    frag = new ControlSectionFragment();
                    break;
                default:
                    frag = new GraphSectionFragment();
            }

            return frag;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "Section " + (position + 1);
        }

    }




    public interface IBluetoothControl {
        public void resumeSelected();
        public void timer();
        public void reload();
        public void StopTimer();
        public void onSelected();
    }

}
