package com.example.denis.watchapp.BluetoothControl;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Denis on 08.08.2014
 */
public class Command {
    public TypeCommand typeCommand;
    public byte[] data;
    public int HeartRate;
    public int ST;
    public int Charge;
    public StateSD SD;
    public Float Power;
    public TypeDevice typeDevice;
    public int SKZ2;
    public Command(byte[] initData, TypeDevice TypeDevice) {
        typeDevice = TypeDevice;
        data = initData;
        if (data[4] == 0x4f && data[5] == 0x4b) {
            typeCommand = TypeCommand.OK;
        } else if (data[1] == 0x03) {

            typeCommand = TypeCommand.ReadStandart;
            ByteBuffer bb = ByteBuffer.wrap(data);
            bb.order(ByteOrder.LITTLE_ENDIAN);
            if(data[10]==3)
            {
                SD = StateSD.Stopped;
            }
            else if (data[10]==0)
            {
                SD = StateSD.Work;
            }
            if (typeDevice == TypeDevice.BM) {
                HeartRate = bb.getShort(4);
            } else if (typeDevice == TypeDevice.IN) {
                Power = ((float) bb.getShort(4)) / 1000;
            } else {
                Power = ((float) bb.getShort(4));
                SKZ2 = bb.getShort(6);
            }
            ST = bb.getShort(6);
            Charge = bb.getShort(8);
            //SD
        } else {
            typeCommand = TypeCommand.Error;

        }


    }

    public static byte[] GetComand(TypeCommand type) {
        byte[] command = new byte[18];
        command[0] = (byte) 0xCC;
        command[1] = 0x0C;
        command[2] = 0;
        command[3] = 18;
        switch (type) {
            case Enable: {
                command[4] = 0x01;
                command[5] = 0x20;
                break;
            }
            case Disable: {
                command[4] = 0x01;
                command[5] = 0x10;
                break;
            }
            case Run: {
                command[4] = 0x02;
                command[5] = 0x20;
                break;
            }
            case Stop: {
                command[4] = 0x02;
                command[5] = 0x10;
                break;
            }
            case TimeSync: {
                command[4] = 0x05;
                Date date = new Date();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                byte hours = (byte) calendar.get(Calendar.HOUR_OF_DAY);
                byte minutes = (byte) calendar.get(Calendar.MINUTE);
                byte seconds = (byte) calendar.get(Calendar.SECOND);
                byte day = (byte) calendar.get(Calendar.DAY_OF_MONTH);
                byte month = (byte) (calendar.get(Calendar.MONTH) + 1);
                byte day_of_week = (byte) (calendar.get(Calendar.DAY_OF_WEEK) - 1);
                byte year = (byte) (calendar.get(Calendar.YEAR) - 2000);
                command[5] = seconds;
                command[6] = minutes;
                command[7] = hours;
                command[8] = day;
                command[9] = day_of_week;
                command[10] = month;
                command[11] = year;
                break;
            }
            case Marker: {
                command[4] = (byte) 0x03;
                String s = "Hello";

                byte[] b = new byte[0];
                try {
                    b = s.getBytes("US-ASCII");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                System.arraycopy(b, 0, command, 5, 5);

                break;
            }
            case ReadStandart: {
                command[4] = (byte) 0x83;

                break;
            }
            case ReadExtendSecond:{
                command[4] = (byte) 0x84;
                command[5] = 0x02;
                break;
            }
            case ReadExtend: {
                command[4] = (byte) 0x84;
                command[5] = 0x01;
                break;
            }
        }
        char control = 0;
        for (int i = 0; i < 15; i++) {
            control += (command[i] & 0x00ff);
        }

        command[15] = (byte) ((control >> 8) & 0xff);
        command[16] = (byte) (control & 0xff);

        command[17] = 0x0A;
        return command;
    }

    /*public static  ExtendData GetExtendData(byte[] data)
    {
        ByteBuffer bb = ByteBuffer.wrap(data);
        ExtendData extendData = new ExtendData();
        extendData.IntData = new long[(data.length-7)/4];
        for (int i=4;i<(data.length-3);i+=4)
        {
            extendData.IntData[i/4-1]=bb.getInt(i) & 0xFFFFFFFFL;
        }
        return extendData;
    };*/

    /*public static  ExtendData GetExtendData(byte[] data)
    {
        ByteBuffer bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        ExtendData extendData = new ExtendData();
        extendData.IntData = new float[(data.length-7)/4];
        for (int i=4;i<(data.length-3);i+=4)
        {
            extendData.IntData[i/4-1]=bb.getFloat(i);
        }
        return extendData;
    };*/
    public static ExtendData GetExtendData(byte[] data) {
        ByteBuffer bb = ByteBuffer.wrap(data);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        ExtendData extendData = new ExtendData();
        extendData.IntData = new short[(data.length - 7) / 2];
        for (int i = 4; i < (data.length - 3) - 4; i += 2) {
            extendData.IntData[i / 2 - 1] = bb.getShort(i);
        }
        return extendData;
    }

    ;
}




