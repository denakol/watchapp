package com.example.denis.watchapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothProfile;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.denis.watchapp.BluetoothControl.BLEService;
import com.example.denis.watchapp.BluetoothControl.Command;
import com.example.denis.watchapp.BluetoothControl.ExtendData;
import com.example.denis.watchapp.BluetoothControl.TypeCommand;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.LineGraphView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by denis on 21.09.2014.
 */
public class GraphSectionFragment extends BluetoothControlFragment {
    private Button btnRun,btnFirst,btnSecond;
    public MyActivity mActivity;
    Timer myTimer;
    private final Object obj = new Object();



    @Override
    public void reload() {
        synchronized (obj)
        {
            Log.d(tag, "перезагрузка  GraphSectionFragment");
            GetBluetoothService().WriteCommand(TypeCommand.ReadExtend);
        }
    }

    @Override
    public void onSelected() {
        Log.d(tag, "выбор  GraphSectionFragment");
        if (GetBluetoothService() != null) {
            GetBluetoothService().setBLEServiceCb(mDCServiceCb);
        }
    }


    public class DCServiceCb extends BluetoothControlFragment.BluetoothCallback {
        @Override
        public void displayCommand(final Command command) {
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        displayCommandHandler(command);

                    }
                });
        }

        @Override
        public void notifyConnectedGATT() {
            if (getActivity() != null)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnRun.setEnabled(true);
                        dialog.dismiss();
                        Toast.makeText(getActivity(), "Подключено", Toast.LENGTH_SHORT).show();
                    }
                });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(tag, "создание  GraphSectionFragment");
        View rootView = inflater.inflate(R.layout.graph, container, false);
        btnRun = (Button) rootView.findViewById(R.id.btnRun);
        btnFirst = (Button) rootView.findViewById(R.id.btnFirst);
        btnSecond = (Button) rootView.findViewById(R.id.btnSecond);
        btnFirst.setVisibility(View.GONE);
        btnSecond.setVisibility(View.GONE);
        mDCServiceCb = new DCServiceCb();
        Log.d("Fragment", "On createview graph");
        btnRun.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                btnRun.setVisibility(View.GONE);
                btnFirst.setVisibility(View.VISIBLE);
                btnSecond.setVisibility(View.VISIBLE);
                GetBluetoothService().WriteCommand(TypeCommand.ReadExtend);

            }
        });
        btnFirst.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                synchronized (obj) {
                    GetBluetoothService().WriteCommand(TypeCommand.ReadExtend);
                }
            }
        });
        btnSecond.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                synchronized (obj) {
                    GetBluetoothService().WriteCommand(TypeCommand.ReadExtendSecond);
                }
            }
        });
        if (mActivity.mBluetoothLeService != null) {
            mActivity.mBluetoothLeService.setBLEServiceCb(mDCServiceCb);
            btnRun.setEnabled(false);
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Подключение");
            dialog.setCancelable(true);
            dialog.show();
        }


        return rootView;
    }

    private void displayCommandHandler(Command command) {
        if (command.typeCommand == TypeCommand.ReadExtend||command.typeCommand == TypeCommand.ReadExtendSecond) {
            ExtendData extendData = Command.GetExtendData(command.data);
            int num = extendData.IntData.length;
            GraphView.GraphViewData[] data = new GraphView.GraphViewData[num];
/*                    Splayn splayn = new Splayn();
                    double[] x = new double[extendData.IntData.length];
                    double [] y = new double[extendData.IntData.length];
                    for (int i = 0; i < num; i++) {

                        x[i] = i;
                        y[i] = extendData.IntData[i];
                    }
                    splayn.BuildSpline(x,y,num);*/
            double v = 0;
            for (int i = 0; i < num; i++) {

                data[i] = new GraphView.GraphViewData(i, extendData.IntData[i]);
            }
            GraphView graphView = new LineGraphView(
                    getActivity()
                    , ""
            );
            graphView.addSeries(new GraphViewSeries(data));
            graphView.setViewPort(2, 40);
            graphView.setScrollable(true);
            graphView.setScalable(true);
            graphView.getGraphViewStyle().setTextSize(12);
            graphView.getGraphViewStyle().setVerticalLabelsWidth(20);
            graphView.getGraphViewStyle().setNumVerticalLabels(5);
            graphView.getGraphViewStyle().setNumHorizontalLabels(20);

            graphView.setManualYAxisBounds((double) getMaxValue(extendData.IntData), (double) getMinValue(extendData.IntData));
            LinearLayout layout = (LinearLayout) getActivity().findViewById(R.id.layout);
            layout.removeViews(0, layout.getChildCount());
            layout.addView(graphView);

            btnFirst.bringToFront();
            btnSecond.bringToFront();
        }
    }

    public  int getMaxValue(short[] array) {
        int maxValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] > maxValue) {
                maxValue = array[i];

            }
        }
        return maxValue;
    }

    public  int getMinValue(short[] array) {
        int minValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < minValue) {
                minValue = array[i];
            }
        }
        return minValue;
    }

    @Override
    public void resumeSelected() {
        Log.d(tag, "возобновление  GraphSectionFragment");
        if (GetBluetoothService() != null) {
            if (GetBluetoothService().getState() == BluetoothProfile.STATE_CONNECTED) {
                btnRun.setEnabled(true);
            } else {
                btnRun.setEnabled(false);
                dialog = new ProgressDialog(getActivity());
                dialog.setMessage("Подключение");
                dialog.setCancelable(true);
                dialog.show();

            }
            onSelected();
        }


    }

    @Override
    public void StopTimer()
    {
        if (myTimer != null) {
            myTimer.cancel();
        }
    }
    @Override
    public void timer() {
        StopTimer();
        myTimer = new Timer();
        myTimer.schedule(new TimerTask() { // Определяем задачу
            @Override
            public void run() {
                synchronized (obj)
                {
                    Log.d(tag, "автоматический запрос  ControlSectionFragment");
                    GetBluetoothService().WriteCommand(TypeCommand.ReadExtend);
                }
            }
        }, 0L,TimeElapse*3);
    }
    public  BLEService.BLEServiceCallback mDCServiceCb;

    @Override
    public void onStop() {

        synchronized (stoppedSync) {
            super.onStop();
            StopTimer();
            connectCount = 0;
            stopped = true;
        }
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MyActivity) activity;
    }
}