package com.example.denis.watchapp;

import android.app.ActionBar;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.denis.watchapp.BluetoothControl.BLEService;
import com.example.denis.watchapp.BluetoothControl.MultipleBLEService;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyActivity extends FragmentActivity implements NavigationDrawerFragment.NavigationDrawerCallbacks {
    public static int CONN_COUNT = 2;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    public BLEService.DeviceBluetoothControl mBluetoothLeService;
    public MultipleBLEService.DeviceBluetoothControl mMultipleBluetoothLeService;
    public FragmentBluetoothModule currentFrag;
    private CharSequence mTitle;
    public BluetoothAdapter mBluetoothAdapter;
    public static int TimeElapse = 2000;
    public static String tag = "user";
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BLEService.DeviceBluetoothControl) service);
            currentFrag.onConnectAction();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    private final ServiceConnection mMultipleBluetoothServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mMultipleBluetoothLeService = ((MultipleBLEService.DeviceBluetoothControl) service);

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mMultipleBluetoothLeService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Устройство не поддерживается", Toast.LENGTH_SHORT).show();
            finish();
        }


        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();


        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
       //
        Intent gattServiceIntent = new Intent(this, BLEService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        Intent MgattServiceIntent = new Intent(this, MultipleBLEService.class);
        bindService(MgattServiceIntent, mMultipleBluetoothServiceConnection, BIND_AUTO_CREATE);
        String locationProvider = LocationManager.NETWORK_PROVIDER;
        String locationProvider1 = LocationManager.GPS_PROVIDER;
// Or use LocationManager.GPS_PROVIDER
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
        Location lastKnownLocation2 = locationManager.getLastKnownLocation(locationProvider);
    }



    @Override
    public void onResume() {
        super.onResume();
        if (!mBluetoothAdapter.isEnabled()) {
            if (!mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.enable();
            }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_update) {
            //open_setting();
            currentFrag.onReloadAction();
            return true;
        }
        if (id == R.id.action_connect) {
            //open_setting();
            currentFrag.onConnectAction();
            return true;
        }
        if (id == R.id.timer) {
            //open_setting();
            currentFrag.onTimer();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        ActionBar actionBar = getActionBar();
        onSectionAttached(position);

        switch (position) {
            case 0: {
                FragmentBM frag = FragmentBM.newInstance(position + 1);
                currentFrag = frag;
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, frag)
                        .commit();
                break;
            }
            case 1: {
                Monitor frag = new Monitor();

                currentFrag = frag;
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, frag)
                        .commit();
                break;
            }
            case 2: {
                FragmentIN frag = FragmentIN.newInstance(position + 1);
                currentFrag = frag;
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, frag)
                        .commit();
                break;
            }
            case 3: {
                FragmentMI frag = FragmentMI.newInstance(position + 1);
                currentFrag = frag;
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, frag)
                        .commit();
                break;
            }
            case 4: {
                getSupportFragmentManager().beginTransaction().replace(R.id.container,
                        new PreferenceFragment()).commit();
                break;
            }

        }
        if(position==1)
        {
            setTheme(R.style.AppThemeDarkBar);
        }
        else
        {
            setTheme(R.style.AppThemeBar);
        }
        actionBar.setTitle(mTitle);

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 0:
                mTitle = getString(R.string.title_section1);
                break;
            case 1:
                mTitle = "Монитор";
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;
            case 3:
                mTitle = "Миографический модуль";
                break;
            case 4:
                mTitle = getString(R.string.title_section3);
                break;

        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.my, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }





    public interface FragmentBluetoothModule {
        public void onReloadAction();
        public void onTimer();
        public void onConnectAction();
    }

}






